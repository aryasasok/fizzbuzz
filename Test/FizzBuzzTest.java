import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FizzBuzzTest {

	@Test
	public void testDivisibleBy3() {
		FizzBuzz b = new FizzBuzz();
		// choose any number that's divisible by 3
		String result = b.buzzz(27);
		assertEquals("fizz", result);
	
		
}
	
	@Test
	public void testDivisibleBy5() {
		FizzBuzz b = new FizzBuzz();
		// choose any number that's divisible by 5
		String result = b.buzzz(10);
		assertEquals("buzz", result);
	
}
	
	@Test
	public void testDivisibleBy3and5() {
		FizzBuzz b = new FizzBuzz();
		// choose any number that's divisible by 5
		String result = b.buzzz(15);
		assertEquals("fizzbuzz", result);
	
}
	
	
	@Test
	public void testOtherNumber() {
		FizzBuzz b = new FizzBuzz();
		// choose any number that's divisible by 4
		String result = b.buzzz(4);
		assertEquals("4", result);
	
}
	
	@Test
	public void testPrimeNumber() {
		FizzBuzz b = new FizzBuzz();
		// choose any number that is prime
		String result = b.buzzz(11);
		assertEquals("whizz", result);
	
}
	//R6 if prime+divisble by 3
	//append WHIZZ to end
	//expectedoutput: 3 => FIZZ WHIZZZ
	//expecteoutput:  5 = > BUZZWIZ
	
	@Test
	public void testAppendWHizz() {
		FizzBuzz b = new FizzBuzz();
		// choose any number that is prime
		String result = b.buzzz(3);
		assertEquals("fizzwhizz", result);
		
	    result = b.buzzz(5);
		assertEquals("buzzwhizz", result);
	
	}
}